package ru.mynewtons.yml.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mynewtons.yml.domain.Product;

public interface ProductRepository extends JpaRepository<Product, String> {
}
