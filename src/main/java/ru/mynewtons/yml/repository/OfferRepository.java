package ru.mynewtons.yml.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.mynewtons.yml.domain.Offer;

public interface OfferRepository extends JpaRepository<Offer, String> {
}
