package ru.mynewtons.yml.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mynewtons.yml.domain.Offer;
import ru.mynewtons.yml.domain.Product;
import ru.mynewtons.yml.service.ProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/")
    public ResponseEntity<List<Product>> findAll(){
        return new ResponseEntity<List<Product>>(productService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Product> save(@RequestBody Product body){
        Product product = productService.save(body);
        return new ResponseEntity<Product>(product,HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id){
        productService.delete(id);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id){
        Optional<Product> optional = productService.findById(id);
        if(optional.isPresent()){
            return new ResponseEntity<Object>(optional.get(),HttpStatus.OK);
        }
        return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
    }
}
