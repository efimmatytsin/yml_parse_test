package ru.mynewtons.yml.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mynewtons.yml.domain.Offer;
import ru.mynewtons.yml.service.OfferService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/offer")
public class OfferController {

    @Autowired
    private OfferService offerService;


    @GetMapping("/")
    public ResponseEntity<List<Offer>> findAll(){
        return new ResponseEntity<List<Offer>>(offerService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Offer> save(@RequestBody Offer body){
        Offer offer = offerService.save(body);
        return new ResponseEntity<Offer>(offer,HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id){
        offerService.delete(id);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id){
        Optional<Offer> offerOptional = offerService.findById(id);
        if(offerOptional.isPresent()){
            return new ResponseEntity<Object>(offerOptional.get(),HttpStatus.OK);
        }
        return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
    }
}
