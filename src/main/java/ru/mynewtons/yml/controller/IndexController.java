package ru.mynewtons.yml.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mynewtons.yml.domain.Offer;
import ru.mynewtons.yml.domain.Product;
import ru.mynewtons.yml.mapper.ModelMapper;
import ru.mynewtons.yml.model.Catalog;
import ru.mynewtons.yml.service.OfferService;
import ru.mynewtons.yml.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/api/index")
public class IndexController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping(value = "/", produces="application/xml", consumes = "application/xml")
    public ResponseEntity<Void> parse(@RequestBody Catalog catalog){
        if(catalog.getShop() != null && catalog.getShop().getOffers() != null) {
            catalog.getShop().getOffers().forEach(offer -> {
                offerService.save(modelMapper.map(offer, Offer.class));
            });
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @GetMapping("/products")
    public ResponseEntity<List<Product>> findProducts(){
        return new ResponseEntity<List<Product>>(productService.findAll(), HttpStatus.OK);
    }

}
