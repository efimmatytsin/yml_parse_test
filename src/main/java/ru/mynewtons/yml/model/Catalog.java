package ru.mynewtons.yml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

@Data
@JacksonXmlRootElement(localName = "yml_catalog")
@XStreamAlias("yml_catalog")
public class Catalog {

    @JacksonXmlProperty(isAttribute = true)
    @XStreamAsAttribute
    private String date;

    private Shop shop;

}
