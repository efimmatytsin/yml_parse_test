package ru.mynewtons.yml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamInclude;
import lombok.*;
import ru.mynewtons.yml.converter.ParamConverter;

@Getter
@Setter
@NoArgsConstructor
@XStreamAlias("param")
@XStreamConverter(ParamConverter.class)
public class Param {

    @JacksonXmlProperty(isAttribute = true)
    @XStreamAsAttribute
    private String name;

    @JacksonXmlText
    private String value;

    public Param(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
