package ru.mynewtons.yml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

@Data
@XStreamAlias("category")
public class Category {

    @JacksonXmlProperty(isAttribute = true)
    @XStreamAsAttribute
    private String id;

    @JacksonXmlText
    private String title;
}
