package ru.mynewtons.yml.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mynewtons.yml.domain.Offer;
import ru.mynewtons.yml.repository.OfferRepository;
import ru.mynewtons.yml.service.OfferService;

import java.util.List;
import java.util.Optional;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private OfferRepository offerRepository;


    @Override
    public Offer save(Offer offer){
        return offerRepository.save(offer);
    }

    @Override
    public Optional<Offer> findById(String id){
        return offerRepository.findById(id);
    }

    @Override
    public void delete(String id){
        offerRepository.deleteById(id);
    }

    @Override
    public List<Offer> findAll(){
        return offerRepository.findAll();
    }
}
