package ru.mynewtons.yml.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mynewtons.yml.domain.Product;
import ru.mynewtons.yml.repository.OfferRepository;
import ru.mynewtons.yml.repository.ProductRepository;
import ru.mynewtons.yml.service.ProductService;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OfferRepository offerRepository;


    @Override
    public List<Product> findAll(){
        return productRepository.findAll();
    }

    @Override
    public void delete(String productId){
        Optional<Product> optional = findById(productId);
        if(optional.isPresent()){
            Product product = optional.get();
            product.getOffers().forEach(offer -> {
                offerRepository.delete(offer);
            });
        }
        productRepository.deleteById(productId);
    }

    @Override
    public Product save(Product product){
        return productRepository.save(product);
    }

    @Override
    public Optional<Product> findById(String productId){
        return productRepository.findById(productId);
    }
}
