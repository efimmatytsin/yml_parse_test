package ru.mynewtons.yml.service;

import ru.mynewtons.yml.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> findAll();

    void delete(String productId);

    Product save(Product product);

    Optional<Product> findById(String productId);
}
