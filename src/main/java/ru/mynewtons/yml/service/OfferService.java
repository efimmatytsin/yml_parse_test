package ru.mynewtons.yml.service;

import ru.mynewtons.yml.domain.Offer;

import java.util.List;
import java.util.Optional;

public interface OfferService {
    Offer save(Offer offer);

    Optional<Offer> findById(String id);

    void delete(String id);

    List<Offer> findAll();
}
