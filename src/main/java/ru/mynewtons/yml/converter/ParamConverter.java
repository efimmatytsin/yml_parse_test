package ru.mynewtons.yml.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.binary.Token;
import ru.mynewtons.yml.model.Param;


public class ParamConverter implements Converter {

    @Override
    public boolean canConvert(Class clazz) {
        return clazz.equals(Param.class);
    }

    @Override
    public void marshal(Object object, HierarchicalStreamWriter writer, MarshallingContext context) {
        Param attribute = (Param) object;
        writer.addAttribute("name", attribute.getName());
        writer.setValue(attribute.getValue());

    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        return new Param(reader.getAttribute("name"), reader.getValue());

    }

}