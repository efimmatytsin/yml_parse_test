package ru.mynewtons.yml.mapper;

import org.modelmapper.*;
import org.springframework.stereotype.Component;
import ru.mynewtons.yml.domain.Product;
import ru.mynewtons.yml.model.Offer;
import ru.mynewtons.yml.model.Param;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ModelMapper {

   public <D> D map(Object source, Class<D> destinationType) {
       org.modelmapper.ModelMapper modelMapper = new org.modelmapper.ModelMapper();

       modelMapper.createTypeMap(Offer.class, ru.mynewtons.yml.domain.Offer.class).setPostConverter(context -> {
           ru.mynewtons.yml.domain.Offer destination = context.getDestination();
           Offer source1 = context.getSource();
           String id = source1.getGroupId() != null ? source1.getGroupId() : source1.getId();
           destination.setProduct(Product.builder()
                   .id(id)
                   .title(source1.getName())
                   .description(source1.getDescription())
                   .build());

           destination.setParams(source1.getParams().stream()
                   .collect(Collectors.toMap(Param::getName, param -> {
                       if(param.getValue() == null){
                           return "";
                       }
                       return param.getValue();
                   }, (s, s2) -> s2))
           );

           return destination;
       });
       return modelMapper.map(source,destinationType);
   }
}
